# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Configuration', 'ConfigurationULProductionType']

SELECTION = ('combined', 'Combined location')


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    combined_start_time_limit = fields.Integer(
        'Location combined start time limit',
        help='Start time limit for location combined in minutes')

    @classmethod
    def __setup__(cls):
        super(Configuration, cls).__setup__()
        if SELECTION not in cls.ul_production_type._field.selection:
            cls.ul_production_type._field.selection.append(SELECTION)


class ConfigurationULProductionType(metaclass=PoolMeta):
    __name__ = 'stock.configuration.ul_production_type'

    @classmethod
    def __setup__(cls):
        super(ConfigurationULProductionType, cls).__setup__()
        if SELECTION not in cls.ul_production_type.selection:
            cls.ul_production_type.selection.append(SELECTION)
