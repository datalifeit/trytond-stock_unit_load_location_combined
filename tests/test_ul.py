# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import doctest_teardown, doctest_checker


class UnitLoadTestCase(ModuleTestCase):
    """Test unit load location combined module"""
    module = 'stock_unit_load_location_combined'

    def setUp(self):
        super(UnitLoadTestCase, self).setUp()


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        UnitLoadTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_unit_load.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
